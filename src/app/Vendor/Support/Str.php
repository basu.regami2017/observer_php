<?php

namespace App\Vendor\Support;

class Str
{

  /**
   * The cache of snake-cased words
   *
   * @var array
   */
   protected static $snakeCache = [];

   /**
    * The cache of camel-cased words
    *
    * @var array
    */
   protected static $cameCache = [];


   /**
    * [Convert a string to snake case]
    * @param  [string] $value     [description]
    * @param  string $delimiter [description]
    * @return [string]            [description]
    */
   public static function snake($value, $delimiter = '_')
   {
      $key = $value;

      if (isset(static::$snakeCache[$key][$delimiter])) {
        return static::$snakeCache[$key][$delimiter];
      }

      if (!ctype_lower($value)) {
        $value = preg_replace('/\s+/u', '', ucwords($value));
      }


   }

}
