<?php
 namespace App\Vendor\Database\Support\Traits;

 use App\Error\Error;
 use App\Error\Exception;
 use App\Vendor\Error\Exception\BadMethodException;

trait ForwardsCallsTrait
{

  protected function forwardCallTo($queryBuilderObject, $methodName, $methodParameters)
  {
    try{
       return $queryBuilderObject->{$methodName}(...$methodParameters);

    } catch (\Throwable $e)  {

      static::BadMethodCallException($queryBuilderObject, $methodName);
    }
  }

  protected static function BadMethodCallException($queryBuilderObject, $methodName)
  {
    throw new BadMethodException('Call to undefine method '. $methodName . ' in '. get_class($queryBuilderObject) . ' class.');
  }
}
