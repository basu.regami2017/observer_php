<?php

use App\Vendor\Environment\DotEnvFactory;
use App\Vendor\Error\Exception\InvalidArgumentException;

if (! function_exists('writeln')) {

  function writeln($line_in) {
      echo $line_in."<br/>";
  }
}

if (! function_exists('dump')) {
  function dump($message)
  {
    var_dump($message);
  }
}

if (! function_exists('env')) {

  function env($key, $default = null, $format = null) {

    static $variables;

    if ($variables === null) {
     $variables = (new DotEnvFactory([new ]))
    }
    var_dump($variables);
    die();
    if (! is_null($format)) {
      if ($format != 'base64') {
        throw new InvalidArgumentException("Unsupported fromat '$format' for decoding env variable '$key' - only base64 is supported. ");
      }
      $value = base64_decode($value);

    }
    return $value;
  }
}
